const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync');


const path = {
    build: {
        html: 'dist/',
        css: 'dist/css/',
        js: 'dist/js/',
        img: 'dist/img/',
        // fonts: 'dist/css/fonts/'
    },
    src: {
        html: 'src/index.html',
        scss: 'src/scss/**/*.scss',
        js: 'src/js/**/*.js',
        img: 'src/img/**/*',
        // fonts: 'src/scss/fonts/*'
    },
    clean: './dist/'
};

const htmlBuild = () => {
    return gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
};
// const fontsBuild = () =>{
//     return gulp.src(path.src.fonts)
//         .pipe(gulp.dest(path.build.fonts))
// };
const imgBuild = () => {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
};

const scssBuild = () => {
    return gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 100 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(path.build.css))
};

const jsBuild = () => {
    return gulp.src(path.src.js)
        .pipe(gulp.dest(path.build.js))
};

const cleanBuild = () => {
    return gulp.src(path.clean, {allowEmpty: true})
        .pipe(clean())
};

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload);
    gulp.watch(path.src.scss, scssBuild).on('change', browserSync.reload);
    gulp.watch(path.src.js, jsBuild).on('change', browserSync.reload);
};
gulp.task('default', gulp.series(
    cleanBuild,
    htmlBuild,
    scssBuild,
    imgBuild,
    jsBuild,
    watcher,
    // fontsBuild
));
gulp.task('build', gulp.series(
    cleanBuild,
    scssBuild,
    htmlBuild,
    imgBuild,
    jsBuild,

    )
);
gulp.task('dev', gulp.watcher);

