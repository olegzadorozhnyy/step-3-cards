import {req} from "../templates/req.js";
import {serializeJSON} from "../functions/index.js";

class Visit {

    constructor(id, doctor, patientName, title, description, urgency ="common", status="open") {
        this._id = id;
        this._doctor = doctor;
        this._patientName = patientName;
        this._title = title;
        this._description = description;
        this._status = status;
        this._urgency = urgency;
        this.elem = null;
    }

    render() {
        const delBtn = document.createElement('button');
        delBtn.innerText = "Delete";
        delBtn.className = 'btn btn-danger';
        delBtn.addEventListener('click', this.delete.bind(this));

        const editBtn = document.createElement('button');
        editBtn.innerText = "Edit";
        editBtn.className = 'btn btn-success';
        editBtn.addEventListener('click', this.edit.bind(this));

        const updateBtn = document.createElement('button');
        updateBtn.innerText = "Update";
        updateBtn.className = 'btn btn-warning d-none';
        updateBtn.addEventListener('click', this.update.bind(this));

        const btnGroup = document.createElement('div');
        btnGroup.className = 'btn-group';
        btnGroup.insertAdjacentElement("afterbegin", updateBtn);
        btnGroup.insertAdjacentElement("afterbegin", delBtn);
        btnGroup.insertAdjacentElement("afterbegin", editBtn);

        const showMore = document.createElement('a');
        showMore.innerText = "Show More";
        showMore.setAttribute('href', `#collapse_${this._id}`)
        showMore.className = 'btn btn-outline-primary my-2';
        // showMore.dataset.target = `#collapse_${this._id}`;
        showMore.dataset.toggle = "collapse";

        this.elem = document.createElement('form');
        this.elem.className = 'card card-body col-6 float-left border-0';
        this.elem.setAttribute('data-id', `${this._id}`);
        this.elem.setAttribute('id', `${this._id}`);
        this.elem.setAttribute('draggable', true);
        this.elem.insertAdjacentElement("afterbegin", btnGroup);


        let status, urgency;
        if (this._status === "done") {status = '<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Status:</span></div><select name="status" class="form-control" disabled><option value="done" selected>Done</option><option value="open">Open</option></select></div>'};
        if (this._status === "open") {status = '<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Status:</span></div><select name="status" class="form-control" disabled><option value="done">Done</option><option value="open" selected>Open</option></select></div>'};
        if (this._urgency === "common") {urgency = '<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Urgency:</span></div><select name="urgency" class="form-control" disabled><option value="common" selected>Common</option><option value="priority">Priority</option><option value="emergency">Emergency</option></select></div>'}
        if (this._urgency === "priority") {urgency = '<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Urgency:</span></div><select name="urgency" class="form-control" disabled><option value="common">Common</option><option value="priority" selected>Priority</option><option value="emergency">Emergency</option></select></div>'}
        if (this._urgency === "emergency") {urgency = '<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Urgency:</span></div><select name="urgency" class="form-control" disabled><option value="common">Common</option><option value="priority">Priority</option><option value="emergency"  selected>Emergency</option></select></div>'}

        let cardText = `
<h5 class="text-center my-3">Visit: ${this._id}</h5>
${status}
<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Name:</span></div><input type="text" name="patientName" value="${this._patientName}" class="form-control" disabled></div>
<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Doctor:</span></div><input type="text" name="doctor" value="${this._doctor}" class="form-control" disabled></div>
<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Title:</span></div><input type="text" name="title" value="${this._title}" class="form-control" disabled></div>
<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Description:</span></div><textarea name="description" value="${this._description}" class="form-control" disabled></textarea></div>
${urgency}
`;

        this.elem.insertAdjacentHTML("beforeend", cardText);
        this.elem.insertAdjacentElement("beforeend", showMore);
    }

    async update(e) {

        e.preventDefault();
        let form = document.getElementById(this._id);
        form.querySelector('.btn-warning').classList.add("d-none");

        let inputs = form.querySelectorAll(".form-control");

        [].forEach.call(inputs, function (item) {
            item.disabled = true;

        });

        let body = serializeJSON(form);
        const data = await req.put(`cards/${this._id}`, body);
        // if (data.status === 200) {
        //     console.log('OK');
        // } else {
        //     console.log('error');
        // }
        //
        // console.log(data.data);

    }

    edit(e) {
        e.preventDefault();

        let form = document.getElementById(this._id);
        form.querySelector('.btn-warning').classList.remove("d-none");

        let inputs = form.querySelectorAll(".form-control");
        [].forEach.call(inputs, function (item) {
            item.removeAttribute("disabled");
        });


    }


    async delete() {
        const pass = prompt('Enter password for removing card (123):');
        if (pass === "123") {
            const data = await req.delete(`cards/${this._id}`);
            if (data.status === 200) {
                this.elem.remove();
            }
        } else {
            alert('Password incorrect!')
        }
    }

}


export class VisitCardiologist extends Visit {
    constructor({id, doctor, patientName, title, description, urgency, status, pressure, massIndex, age, medicalHistory} = {}) {
        super(id, doctor, patientName, title, description, urgency, status);
        this._pressure = pressure;
        this._massIndex = massIndex;
        this._medicalHistory = medicalHistory;
        this._age = age;
    }

    render(place) {
        super.render();
        const text = `
<div class="collapse" id="collapse_${this._id}">
<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Blood pressure:</span></div><input type="text" name="pressure" value="${this._pressure}" class="form-control" disabled></div>
<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Mass index:</span></div><input type="text" name="massIndex" value="${this._massIndex}" class="form-control" disabled></div>
<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Diseases:</span></div><input type="text" name="medicalHistory" value="${this._medicalHistory}" class="form-control" disabled></div>        
<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Age:</span></div><input type="text" name="age" value="${this._age}" class="form-control" disabled></div>
</div>
        `;
        this.elem.insertAdjacentHTML("beforeend", text);
        return this.elem;

    }
}


export class VisitDentist extends Visit {
    constructor({id, doctor, patientName, title, description, urgency, status, lastVisit} = {}) {
        super(id, doctor, patientName, title, description, urgency, status);
        this._lastVisit = lastVisit;
    }

    render() {
        super.render();
        const text = `
<div class="collapse" id="collapse_${this._id}"> 
<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Date of last visit:</span></div><input type="text" name="lastVisit" value="${this._lastVisit}" class="form-control" disabled></div>
</div>
`;
        this.elem.insertAdjacentHTML("beforeend", text);
        return this.elem;
    }
}


export class VisitTherapist extends Visit {
    constructor({id, doctor, patientName, title, description, urgency, status, age} = {}) {
        super(id, doctor, patientName, title, description, urgency, status);
        this._age = age;
    }

    render() {
        super.render();
        const text = `
<div class="collapse" id="collapse_${this._id}">
<div class="input-group mb-1"><div class="input-group-prepend"><span class="input-group-text">Age:</span></div><input type="text" value="${this._age}" class="form-control" disabled></div>
</div>
`;
        this.elem.insertAdjacentHTML("beforeend", text);
        return this.elem;
    }
}

export default {VisitTherapist, VisitDentist, VisitCardiologist}