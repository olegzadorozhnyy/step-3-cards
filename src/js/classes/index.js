import Form from "./form.js";
import {ModalAddCard, ModalLogin} from "./modal.js";
import {VisitTherapist, VisitDentist, VisitCardiologist} from "./visit.js";

export {Form, ModalLogin, ModalAddCard, VisitTherapist, VisitDentist, VisitCardiologist};
