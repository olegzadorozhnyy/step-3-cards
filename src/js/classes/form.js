export default class Form {
    constructor({id, title, inputs, buttons, onlyInputs}) {
        this.elem = null;
        this.title = title || false;
        this.id = id;
        this.inputs = inputs;
        this.buttons = buttons;
        this.onlyInputs = onlyInputs || false;
    }
    render() {
        this.elem = document.createElement("form");
        this.elem.id = this.id;
        this.elem.insertAdjacentHTML("beforeend", this.renderBody());
        const additionalFields = `<div class="additional-fields"></div>`;
        this.elem.insertAdjacentHTML("beforeend", additionalFields);
        if(this.buttons) {
            this.elem.insertAdjacentHTML("beforeend", this.renderButtons());
        }
        if(this.onlyInputs) {
            this.elem = this.elem.childNodes;
            console.log(this.elem);
        }
        if(this.title) {
            const formTitle = `<div class="row mb-4">
                                    <div class="col text-center">
                                        <h1>${this.title}</h1>
                                    </div>
                                </div>`;
            this.elem.insertAdjacentHTML("afterbegin", formTitle);
        }
        return this.elem;
    }
    renderBody(){
        return this.inputs.map((item) => {
            let body;

            if (item.type === "text" || item.type === "password" || item.type === "email") {
                body = new BaseInput(item);
            }
            if (item.type === "select") {
                body = new SelectInput(item);
            }
            return body.render().outerHTML;
        }).join("");
    }
    renderButtons() {
        return this.buttons.map(({type, id, btnClass, text}) => {
            const button = document.createElement("button");
            button.className = btnClass;
            if(id) {
                button.id = id;
            }
            button.type = type;
            button.innerText = text;
            return button.outerHTML;
        }).join("");
    }
}



class Input {
    constructor({label}) {
        this.elem = null;
        this.input = null;
        this.label = label || false;
    }
    render() {
        this.elem = document.createElement("div");
        this.elem.className = "form-group";

        if (this.label) {
            this.elem.insertAdjacentHTML("afterBegin", `<label>${this.label}</label>`);
        }
    }
}

class BaseInput extends Input{
    constructor({placeholder, name, type, requiered, ...attr}) {
        super(attr);
        this.name = name;
        this.type = type;
        this.requiered = requiered || "";
        this.placeholder = placeholder || "";
    }
    render() {
        super.render();

        this.input = `<input type="${this.type}" name="${this.name}" placeholder="${this.placeholder}" class="form-control" ${this.requiered}>`;
        this.elem.insertAdjacentHTML("beforeend", this.input);
        return this.elem;
    }
}

class SelectInput extends Input {
    constructor({id, name, dataAttr, options, ...attr}) {
        super(attr);
        this.name = name;
        this.id = id || false;
        this.dataAttr = dataAttr || false;
        this.options = options;
    }
    render() {
        super.render();
        this.input = document.createElement("select");
        this.input.name = this.name;
        this.input.className = "form-control";
        if(this.id) {
            this.input.id = this.id;
        }
        if(this.dataAttr) {
            this.input.setAttribute('data-target', this.dataAttr);
        }
        const body = this.options.map(({value, disabled = false, selected = false, text}) => {
            const option = document.createElement("option");
            option.value = value;
            if(selected) {
                option.setAttribute('selected', 'true');
            }
            if(disabled) {
                option.setAttribute('disabled', 'true');
            }
            option.innerText = text;
            return option.outerHTML;
        }).join("");
        this.input.insertAdjacentHTML("afterbegin", body);
        this.elem.insertAdjacentHTML("beforeend", this.input.outerHTML);
        return this.elem;
    }
}

