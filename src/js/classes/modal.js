import {addCard, loginUser, showAdditionalFields} from "../functions/index.js";
import Form from "./form.js";
import {formTemplate, htmlTemplate} from "./../templates/template.js";

class Modal {
    constructor() {
        this.elem = null;
    }
    render(){
        this.elem = document.createElement("div");
        this.elem.className = "popup";
        this.elem.innerHTML = htmlTemplate.modal;
        this.elem.addEventListener("click", ({target}) => {
            if (target === this.elem.querySelector(".close") || target === this.elem.querySelector("#cancel")) {
                this.elem.remove();
            }
        });
        const wrapper = document.querySelector(".wrapper");
        wrapper.insertAdjacentElement('afterBegin', this.elem);
    }
}

export class ModalAddCard extends Modal {
    constructor() {
        super();
    }
    render() {
        super.render();

        const modalContent = this.elem.querySelector(".popup-content");
        const form = new Form(formTemplate.addCardForm);
        modalContent.insertAdjacentHTML('beforeEnd', form.render().outerHTML);

        const selectDoctor = document.getElementById('select-doctor');
        selectDoctor.addEventListener('change', showAdditionalFields);

        const cardForm = document.getElementById('card-form');
        cardForm.addEventListener('submit', addCard);


    }
}

export class ModalLogin extends Modal {
    constructor() {
        super();
    }
    render() {
        super.render();
        const modalContent = this.elem.querySelector(".popup-content");
        const form = new Form(formTemplate.loginForm);
        modalContent.insertAdjacentElement('beforeEnd', form.render());
        const loginForm = document.getElementById('login-form');
        if (loginForm) {
            loginForm.addEventListener('submit', loginUser);
        }
    }
}

export default {ModalAddCard, ModalLogin}
