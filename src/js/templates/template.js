export const htmlTemplate = {
    modal: `<div class="popup-content">
            <span class="close">&times;</span>
        </div>`,
    btnLogin: `<button class="btn btn-primary" id="login">Вход</button>`,
    btnAddCard: `<button class="btn btn-primary" id="add-card">Добавить карточку</button>`,
};

export const formTemplate = {
    searchForm : {
        id: "search-form",
        title : false,
        inputs: [
            {
                type: "text",
                name: "find-by-title",
                placeholder: "Поиск по Title"
            },
            {
                type: "select",
                name : "status-search",
                id : "status-search",
                dataAttr : false,
                options : [
                    {
                        value: "",
                        disabled : true,
                        selected : true,
                        text : "Статус выполнения"
                    },
                    {
                        value: "open",
                        disabled : false,
                        selected : false,
                        text : "В процессе"
                    },{
                        value: "done",
                        disabled : false,
                        selected : false,
                        text : "Выполнено"
                    },
                ]
            },
            {
                type: "select",
                name : "priority-search",
                id : "priority-search",
                dataAttr : false,
                options : [
                    {
                        value: "",
                        disabled : true,
                        selected : true,
                        text : "Приоритетность"
                    },
                    {
                        value: "common",
                        disabled : false,
                        selected : false,
                        text : "Обычная"
                    },
                    {
                        value: "priority",
                        disabled : false,
                        selected : false,
                        text : "Приоритетная"
                    },
                    {
                        value: "emergency",
                        disabled : false,
                        selected : false,
                        text : "Неотложная"
                    },
                ]
            },
        ],
        buttons : [
            {
                type : "submit",
                id : "search-btn",
                btnClass: "btn btn-primary mt-3",
                text : "Поиск"
            }
        ]
    },
    loginForm : {
        id: "login-form",
        title : "Login",
        inputs: [
            {
                type: "email",
                name: "email",
                placeholder: "Email",
                required : "required"
            },
            {
                type: "password",
                name: "password",
                placeholder: "Password",
                required : "required"
            }
        ],
        buttons : [
            {
                type : "submit",
                id : false,
                btnClass: "btn btn-primary mt-4",
                text : "Login"
            }
        ]
    },
    addCardForm : {
        id: "card-form",
        title : "Add card",
        inputs: [
            {
                type: "select",
                name : "doctor",
                id : "select-doctor",
                dataAttr : ".additional-fields",
                options : [
                    {
                        value: "",
                        disabled : true,
                        selected : true,
                        text : "Выберите врача"
                    },
                    {
                        value: "cardio",
                        disabled : false,
                        selected : false,
                        text : "Кардиолог"
                    },
                    {
                        value: "dentist",
                        disabled : false,
                        selected : false,
                        text : "Стоматолог"
                    },{
                        value: "therapist",
                        disabled : false,
                        selected : false,
                        text : "Терапевт"
                    },
                ]
            },
        ],
        buttons : [
            {
                type : "submit",
                id : false,
                btnClass: "btn btn-success",
                text : "Создать"
            },
            {
                type : false,
                id : "cancel",
                btnClass: "btn btn-secondary",
                text : "Закрыть"
            }
        ]
    },
    cardio : {
        id: "else",
        title : false,
        inputs: [
            {
                type: "text",
                label : "Имя и фамилия",
                name: "patientName",
                placeholder: ""
            },
            {
                type: "text",
                label : "Краткое описание визита",
                name: "title",
                placeholder: ""
            },
            {
                type: "text",
                label : "Цель визита",
                name: "description",
                placeholder: ""
            },
            {
                type: "text",
                label : "Обычное давление",
                name: "pressure",
                placeholder: ""
            },
            {
                type: "text",
                label : "Индекс массы тела",
                name: "massIndex",
                placeholder: ""
            },
            {
                type: "text",
                label : "Возраст",
                name: "age",
                placeholder: ""
            },
            {
                type: "select",
                label : "Приоритетность",
                name : "priority-search",
                id : "priority-search",
                dataAttr : false,
                options : [
                    {
                        value: "common",
                        disabled : false,
                        selected : true,
                        text : "Обычная"
                    },
                    {
                        value: "priority",
                        disabled : false,
                        selected : false,
                        text : "Приоритетная"
                    },
                    {
                        value: "emergency",
                        disabled : false,
                        selected : false,
                        text : "Неотложная"
                    },
                ]
            },
            {
                type: "text",
                label : "Перенесенные заболевания сердечно-сосудистой системы",
                name: "medicalHistory",
                placeholder: ""
            },

        ],
        buttons : false
    },
    dentist : {
        id: "else",
        title : false,
        inputs: [
            {
                type: "text",
                label : "Имя и фамилия",
                name: "patientName",
                placeholder: ""
            },
            {
                type: "text",
                label : "Краткое описание визита",
                name: "title",
                placeholder: ""
            },
            {
                type: "text",
                label : "Цель визита",
                name: "description",
                placeholder: ""
            },
            {
                type: "text",
                label : "Дата последнего посещения",
                name: "lastVisit",
                placeholder: ""
            },
            {
                type: "select",
                label : "Приоритетность",
                name : "priority-search",
                id : "priority-search",
                dataAttr : false,
                options : [
                    {
                        value: "common",
                        disabled : false,
                        selected : true,
                        text : "Обычная"
                    },
                    {
                        value: "priority",
                        disabled : false,
                        selected : false,
                        text : "Приоритетная"
                    },
                    {
                        value: "emergency",
                        disabled : false,
                        selected : false,
                        text : "Неотложная"
                    },
                ]
            },
        ],
        buttons : false
    },
    therapist : {
        id: "else",
        title : false,
        inputs: [
            {
                type: "text",
                label : "Имя и фамилия",
                name: "patientName",
                placeholder: ""
            },
            {
                type: "text",
                label : "Краткое описание визита",
                name: "title",
                placeholder: ""
            },
            {
                type: "text",
                label : "Цель визита",
                name: "description",
                placeholder: ""
            },
            {
                type: "text",
                label : "Возраст",
                name: "age",
                placeholder: ""
            },
            {
                type: "select",
                label : "Приоритетность",
                name : "priority-search",
                id : "priority-search",
                dataAttr : false,
                options : [
                    {
                        value: "common",
                        disabled : false,
                        selected : true,
                        text : "Обычная"
                    },
                    {
                        value: "priority",
                        disabled : false,
                        selected : false,
                        text : "Приоритетная"
                    },
                    {
                        value: "emergency",
                        disabled : false,
                        selected : false,
                        text : "Неотложная"
                    },
                ]
            },
        ],
        buttons : false
    },
};
