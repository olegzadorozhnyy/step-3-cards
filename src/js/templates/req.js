import {checkToken} from "../functions/index.js";

export const req = axios.create({
    baseURL: 'http://cards.danit.com.ua/',
    headers: {
        Authorization: `Bearer ${checkToken()}`
    }
});
