import {VisitCardiologist, VisitDentist, VisitTherapist} from "../classes/index.js";

export default function renderCards(data) {
    let wrapper = document.getElementById("card-wrapper");
    data.forEach(item => {
        let visit;
        if (item.doctor === 'cardio')  visit = new VisitCardiologist(item).render();
        if (item.doctor === 'dentist')  visit = new VisitDentist(item).render();
        if (item.doctor === 'therapist') visit = new VisitTherapist(item).render();
        if (visit)  wrapper.insertAdjacentElement("beforeend", visit);
    })

}
