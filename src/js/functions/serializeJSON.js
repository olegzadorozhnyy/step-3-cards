export default function serializeJSON (form) {
    const body = {};
    form.querySelectorAll('input,  select, textarea').forEach(element => {
        const name = element.getAttribute('name');
        if(name) {
            body[name] = element.value;
        }
    });
    return body;
    console.log(body)
}