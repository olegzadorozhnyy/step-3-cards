import {htmlTemplate, formTemplate} from "./../templates/template.js";
import Form from "../classes/form.js";

export default function showAdditionalFields(event){
    this.children[0].setAttribute('disabled', 'true');
    const {target} = this.dataset;
    const inputs = new Form(formTemplate[this.value]);

    this.closest('form').querySelector(target).innerHTML = inputs.render().outerHTML;
    //this.closest('form').querySelector(target).innerHTML = htmlTemplate[this.value];
}
