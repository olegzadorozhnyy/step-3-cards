import {renderCards, checkToken} from "./index.js";

export default async function getCards() {
    const req = axios.create({
        baseURL: 'http://cards.danit.com.ua/',
        headers: {
            Authorization: `Bearer ${checkToken()}`
        }
    });
    const {data} = await req.get('cards');
    renderCards(data);

}
