import {serializeJSON, renderPage} from "./index.js";
import {req} from "./../templates/req.js";

export default async function loginUser(event){
    event.preventDefault();
    const body = serializeJSON(this);
    const {data} = await req.post('/login', body);

    if (data.status === 'Success') {
        localStorage.setItem('token', data.token);
        this.closest(".popup").remove();
        renderPage();
    } else {
        this.insertAdjacentHTML('beforeEnd', `<p>${data.message}</p>`)
    }
}
