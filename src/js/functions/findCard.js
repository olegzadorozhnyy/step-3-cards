import {renderCards, checkToken} from "./index.js";
export default async function findCard() {
    const req = axios.create({
        baseURL: 'http://cards.danit.com.ua/',
        headers: {
            Authorization: `Bearer ${checkToken()}`
        }
    });
    const {data} = await req.get('/cards');
    let wrapper = document.getElementById("card-wrapper");
    console.log(data);
    let title = document.querySelector(`[name = "find-by-title"]`);
    let status = document.querySelector(`[name = "status-search"]`);
    let priority = document.querySelector(`[name = "priority-search"]`);
    const searchBtn = document.getElementById("search-btn");
    searchBtn.addEventListener("click", function () {
        wrapper.innerHTML = "";

        const results = data.filter((element)=>{
            const {title,status,urgency} = element;
            return (title === title.value || (!title.value)) && (status === status.value) && (urgency === priority.value);
        });


        if (title.value !== "" && status.value === "" && priority.value === "") {
            data.forEach (function (item) {
                if (item.title === title.value) {
                    console.log("Match");
                    renderCards([item]);
                }
            })
        }
        if (title.value !== ""  && status.value !== "" && priority.value === "") {
            data.forEach (function (item) {
                if (item.title === title.value && item.status === status.value) {
                    console.log("Match");
                    renderCards([item]);
                }
            })
        }
        if (title.value !== ""  && status.value !== "" && priority.value !== "") {
            data.forEach (function (item) {
                if (item.title === title.value && item.status === status.value && item.urgency === priority.value) {
                    console.log("Match");
                    renderCards([item]);
                }
            })
        }
        if (title.value === ""  && status.value !== "" && priority.value !== "") {
            data.forEach (function (item) {
                if (item.status === status.value && item.urgency === priority.value) {
                    console.log("Match");
                    renderCards([item]);
                }
            })
        }
        if (title.value === ""  && status.value === "" && priority.value !== "") {
            data.forEach (function (item) {
                if (item.urgency === priority.value) {
                    console.log("Match");
                    renderCards([item]);
                }
            })
        }
        if (title.value === ""  && status.value !== "" && priority.value === "") {
            data.forEach (function (item) {
                if (item.status === status.value) {
                    console.log("Match");
                    renderCards([item]);
                }
            })
        }
        if (title.value !== ""  && status.value === "" && priority.value !== "") {
            data.forEach (function (item) {
                if (item.title === title.value && item.urgency === priority.value) {
                    console.log("Match");
                    renderCards([item]);
                }
            })
        }
    });
}


