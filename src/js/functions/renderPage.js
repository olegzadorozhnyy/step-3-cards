import {checkToken, getCards} from "./index.js";
import {htmlTemplate, formTemplate} from ".././templates/template.js";
import {Form, ModalLogin, ModalAddCard} from ".././classes/index.js";


export default function renderPage () {
    const header = document.querySelector(".header .buttons");


    if (checkToken()) {
        header.innerHTML = htmlTemplate.btnAddCard;
        const addCardBtn = document.getElementById("add-card");
        addCardBtn.addEventListener("click", function () {
            const modal = new ModalAddCard();
            modal.render();
        });

        const findCard = document.getElementById("find-form");

        const newForm = new Form(formTemplate.searchForm);
        findCard.insertAdjacentElement("afterbegin", newForm.render());

        const searchBtn = document.getElementById("search-form");
        searchBtn.addEventListener("submit", function (event) {
            event.preventDefault();
        });
        getCards();
}
    else {
        header.innerHTML = htmlTemplate.btnLogin;
        const loginBtn = document.getElementById("login");
        loginBtn.addEventListener("click", function () {
            const modal = new ModalLogin();
            modal.render();
        });
    }
}
