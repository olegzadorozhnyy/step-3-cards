import {serializeJSON} from "./../functions/index.js";
import {VisitTherapist, VisitDentist, VisitCardiologist} from "./../classes/index.js"
import {checkToken} from "./../functions/index.js";

export default async function addCard (event) {
    event.preventDefault();


    const wrapper = document.getElementById("card-wrapper");
    const body = serializeJSON(this);
    const req = axios.create({
        baseURL: 'http://cards.danit.com.ua/',
        headers: {
            Authorization: `Bearer ${checkToken()}`
        }
    });
    const {data} = await req.post('cards', body);

    let visit;
    if (body.doctor === 'cardio') visit = new VisitCardiologist(data);
    if (body.doctor === 'dentist') visit = new VisitDentist(data);
    if (body.doctor === 'therapist') visit = new VisitTherapist(data);
    wrapper.insertAdjacentElement("beforeend", visit.render());

    let modal = document.querySelector('.popup');
    modal.remove();
}

